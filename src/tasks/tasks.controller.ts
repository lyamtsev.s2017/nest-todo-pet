import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetFilteredTasksDto } from './dto/get-filtered-tasks.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { TaskEntity } from './task.entity';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { UserEntity } from '../auth/user.entity';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
  constructor(private tasksService: TasksService) {}

  @Get()
  getTasks(
    @Query() dto: GetFilteredTasksDto,
    @GetUser() user: UserEntity,
  ): Promise<TaskEntity[]> {
    return this.tasksService.getTasks(dto, user);
  }

  @Get(':id')
  getTaskById(
    @Param('id', ParseUUIDPipe) id: TaskEntity['id'],
    @GetUser() user: UserEntity,
  ): Promise<TaskEntity> {
    return this.tasksService.getTaskById(id, user);
  }

  @Post()
  async createTask(
    @Body() createTaskDto: CreateTaskDto,
    @GetUser() user: UserEntity,
  ): Promise<TaskEntity> {
    return await this.tasksService.createTask(createTaskDto, user);
  }

  @Delete(':id')
  async deleteTask(
    @Param('id', ParseUUIDPipe) id: TaskEntity['id'],
    @GetUser() user: UserEntity,
  ): Promise<{ id: TaskEntity['id'] }> {
    const taskId = await this.tasksService.deleteTask(id, user);

    return {
      id: taskId,
    };
  }

  @Patch(':id/status')
  async updateTaskStatus(
    @Body() updateTaskStatusDTO: UpdateTaskStatusDto,
    @Param('id') id: TaskEntity['id'],
    @GetUser() user: UserEntity,
  ): Promise<TaskEntity> {
    return this.tasksService.updateTaskStatus(id, updateTaskStatusDTO, user);
  }
}
