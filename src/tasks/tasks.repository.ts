import { Repository } from 'typeorm';
import { TaskEntity } from './task.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { GetFilteredTasksDto } from './dto/get-filtered-tasks.dto';
import { Injectable } from '@nestjs/common';
import { UserEntity } from '../auth/user.entity';

@Injectable()
export class TasksRepository extends Repository<TaskEntity> {
  constructor(
    @InjectRepository(TaskEntity)
    private tasksRepository: Repository<TaskEntity>,
  ) {
    super(
      tasksRepository.target,
      tasksRepository.manager,
      tasksRepository.queryRunner,
    );
  }

  async getTasks(
    dto: GetFilteredTasksDto,
    user: UserEntity,
  ): Promise<TaskEntity[]> {
    const { status, search } = dto;

    const query = this.createQueryBuilder('task');

    query.where({ user });

    if (status) {
      query.andWhere('task.status = :status', { status });
    }

    if (search) {
      query.andWhere(
        'task.title ILIKE :search OR task.description ILIKE :search ',
        { search: `%${search}%` },
      );
    }

    return query.getMany();
  }

  async createTask(dto: CreateTaskDto, user: UserEntity): Promise<TaskEntity> {
    const task = await this.create({
      ...dto,
      user,
      status: TaskStatus.OPEN,
    });

    await this.save(task);

    return task;
  }
}
