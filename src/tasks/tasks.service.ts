import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetFilteredTasksDto } from './dto/get-filtered-tasks.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { TasksRepository } from './tasks.repository';
import { TaskEntity } from './task.entity';
import { UserEntity } from '../auth/user.entity';

@Injectable()
export class TasksService {
  constructor(private tasksRepository: TasksRepository) {}

  async getTasks(
    dto: GetFilteredTasksDto,
    user: UserEntity,
  ): Promise<TaskEntity[]> {
    return this.tasksRepository.getTasks(dto, user);
  }

  createTask(dto: CreateTaskDto, user: UserEntity): Promise<TaskEntity> {
    return this.tasksRepository.createTask(dto, user);
  }

  async getTaskById(
    id: TaskEntity['id'],
    user: UserEntity,
  ): Promise<TaskEntity> {
    const task = await this.tasksRepository.findOneBy({ id, user });

    if (!task) {
      throw new NotFoundException(`Task with id ${id} not found.`);
    }

    return task;
  }

  async deleteTask(id: TaskEntity['id'], user): Promise<TaskEntity['id']> {
    const result = await this.tasksRepository.delete({ id, user });

    if (result.affected === 0) {
      throw new NotFoundException(`Task with id ${id} not found.`);
    }

    return id;
  }

  async updateTaskStatus(
    id: TaskEntity['id'],
    dto: UpdateTaskStatusDto,
    user: UserEntity,
  ): Promise<TaskEntity> {
    const task = await this.getTaskById(id, user);

    task.status = dto.status;

    await this.tasksRepository.save(task);

    return task;
  }
}
